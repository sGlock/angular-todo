import { Injectable } from '@angular/core';
import * as R from 'ramda';
import { Subject } from 'rxjs/Subject';

@Injectable({
  providedIn: 'root'
})
export class TodosService {

  constTodos = [
  	  {
  	  	"id": 1,
	    "name": "Incredible Concrete Fish",
	    "text": "Labore quaerat dolorem nihil voluptas magni velit autem.",
	    "date": 1551827169801
	  },
	  {
	  	"id": 2,
	    "name": "Refined Wooden Chair",
	    "text": "Pariatur assumenda reprehenderit suscipit nam voluptas atque.",
	    "date": 1532491668401
	  },
	  {
	  	"id": 3,
	    "name": "Intelligent Rubber Shirt",
	    "text": "Dolore eveniet totam quia consequatur fuga incidunt.",
	    "date": 1548157224126
	  },
	  {
	  	"id": 4,
	    "name": "Handcrafted Wooden Fish",
	    "text": "Consectetur non omnis facilis ratione.",
	    "date": 1552737407573
	  },	  
	  {
	  	"id": 5,
	    "name": "Intelligent Cotton Towels",
	    "text": "Odio recusandae ut ea laborum sunt illo laboriosam id et.",
	    "date": 1536265006542
	  },
	  {
	  	"id": 6,
	    "name": "Fantastic Concrete Gloves",
	    "text": "Et ut nisi ut.",
	    "date": 1536265006542
	  },
	  {
	  	"id": 7,
	    "name": "Generic Wooden Keyboard",
	    "text": "Fugit libero veritatis nostrum magnam est adipisci sunt.",
	    "date": 1536265006542
	  },
	  {
	  	"id": 8,
	    "name": "Refined Granite Salad",
	    "text": "Pariatur id provident.",
	    "date": 1552737407573
	  },
	  {
	  	"id": 9,
	    "name": "Handcrafted Frozen Chair",
	    "text": "Quisquam eos ex illo facere consequatur quo dignissimos itaque voluptate.",
	    "date": 1552737407573
	  },
	  {
	  	"id": 10,
	    "name": "Small Metal Chips",
	    "text": "Atque et quae.",
	    "date": 1551827169801
	  },
	  {
	  	"id": 11,
	    "name": "Unbranded Granite Cheese",
	    "text": "Rerum earum voluptas est aperiam blanditiis.",
	    "date": 1548157224126
	  },	  
	  {
	  	"id": 12,
	    "name": "Handcrafted Rubber Computer",
	    "text": "Qui corporis voluptas voluptas quis ipsa autem qui cupiditate.",
	    "date": 1548157224126
	  },
	  {
	  	"id": 13,
	    "name": "Handcrafted Metal Shoes",
	    "text": "Iure error ullam.",
	    "date": 1548157224126
	  },
	  {
	  	"id": 14,
	    "name": "Generic Steel Hat",
	    "text": "Dolorem dicta nostrum corporis qui qui recusandae aspernatur voluptatem.",
	    "date": 1532491668401
	  },
	  {
	  	"id": 15,
	    "name": "Gorgeous Cotton Soap",
	    "text": "Voluptates dignissimos placeat et iusto deserunt amet.",
	    "date": 1548157224126
	  },
	  {
	  	"id": 16,
	    "name": "Handmade Metal Computer",
	    "text": "Cumque quis incidunt.",
	    "date": 1532491668401
	  },
	  {
	  	"id": 17,
	    "name": "Fantastic Cotton Pants",
	    "text": "Consequuntur ut nisi omnis ut quas.",
	    "date": 1551827169801
	  },
	  {
	  	"id": 18,
	    "name": "Rustic Plastic Pizza",
	    "text": "Non odit ducimus quis ex nihil.",
	    "date": 1551827169801
	  },	  
	  {
	  	"id": 19,
	    "name": "Handmade Frozen Car",
	    "text": "Blanditiis voluptas harum perspiciatis reprehenderit molestiae autem iste sapiente.",
	    "date": 1536265006542
	  },
	  {
	  	"id": 20,
	    "name": "Handcrafted Cotton Ball",
	    "text": "Quo reprehenderit minima dolorum esse consequatur.",
	    "date": 1536265006542
	  }
  ];
  
  editTodoSubscribe = new Subject();
  deleteTodoSubscribe = new Subject();

  constructor() { }

  getTodos(){
  	return this.constTodos.slice();
  }

  addTodo(todo){
  	this.constTodos.push({
  		id: R.reduce(R.max, -Infinity, R.pluck('id')(this.constTodos)) + 1,
  		name: todo.name,
  		text: todo.text,
  		date: Date.now()
  	});
  }

  deleteTodo(id){
  	this.constTodos = this.constTodos.filter((item) => {return item.id !== id});
  	this.deleteTodoSubscribe.next();
  }

  filterName(searchName, todos = this.constTodos){
  	return todos.filter((todo) => 
		todo['name'].toUpperCase().indexOf(searchName.toUpperCase()) !== -1);  	
  }

  filterDate(searchDate, todos = this.constTodos){
  	return todos.filter((todo) => 
		todo['date'] == searchDate);
  }

  getUniqDates(){
  	return R.uniq(R.pluck('date')(this.constTodos));
  }

  editTodo(todo){
  	this.constTodos[todo.id - 1].name = todo.name;
  	this.constTodos[todo.id - 1].text = todo.text;
  	this.editTodoSubscribe.next();  	
  }  

}
