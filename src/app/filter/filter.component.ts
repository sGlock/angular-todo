import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Input() todosDates: any;
  @Output() onFilterName = new EventEmitter<{name: string}>();
  @Output() onFilterDate = new EventEmitter<{date: any}>();

  todoName = '';
  todoDate = '';

  filterName(){    
  	this.onFilterName.emit({
  		name: this.todoName
  	});
  }

  filterDate(){    
    this.onFilterDate.emit({
      date: this.todoDate
    });
  }

}
