import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  @Input() todos: {id: number, name: string, text: string, date: any}
  @Input() pagFirst: number
  @Input() pagLast: number
   
  constructor() { }

  ngOnInit() {
  }

}
