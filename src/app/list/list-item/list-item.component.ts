import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { TodosService } from '../../todos.service';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css']
})
export class ListItemComponent implements OnInit {

  @Input() todo: {id: number, name: string, text: string, date: any};

  constructor(private todosService: TodosService) { } 

  ngOnInit() {
  }

  editStatus = 0;
  name = '';
  text = '';  

  deleteTodo(id){
    this.todosService.deleteTodo(id);
  }

  editTodo(){
  	this.editStatus = 1;
  	this.name = this.todo.name;
    this.text = this.todo.text;  	
  }

  cancelEditTodo(){
  	this.editStatus = 0;  	
  }

  editTodoSave(id){   	
  	let editTodo = {
  		id: id,
  		name: this.name,
  		text: this.text
  	}
  	
    this.todosService.editTodo(editTodo);
  	this.editStatus = 0;	
  }  

}
