import { Component, OnInit } from '@angular/core';
import { TodosService } from './todos.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {	
    todos = [];
    todosDates = [];
    activeIndex = 0;

    filterNameValue: string = '';
    filterDateValue: string = '';

	pagStep: number = 6;
  	pagCount = [];
	pagFirst = 0;
	pagLast = 6;	

	constructor(private todosService: TodosService) { 

		/* events from deep child component */

		/* refresh todos when edit */
		this.todosService.editTodoSubscribe.subscribe(
	      () => {
	        this.todos = this.todosService.getTodos();

	        /* when todos filtered */
	        if(this.filterNameValue !== '' || this.filterDateValue !== ''){
				this.filterTodo(false, this.activeIndex, this.pagCount.length);
				return;
			}
	      }
    	);  

		/* delete todos */
    	this.todosService.deleteTodoSubscribe.subscribe(
	      () => {
	      	this.todos = this.todosService.getTodos();
		  	this.checkPagination();

		  	/* when delete filtered todos */
		  	if(this.filterNameValue !== '' || this.filterDateValue !== ''){
				this.filterTodo(false, this.activeIndex, this.pagCount.length);	
				return;		
			}	  	

			/* when delete todos, todos empty on page and need go to prev page sheet */
			if(this.todos.length % this.pagStep === 0 && this.activeIndex >= this.pagCount.length){						
				this.goPagination(this.pagCount.length);
				this.activeClass(this.pagCount.length - 1);						
			}	
	      }
	     );

	}	
	
	ngOnInit(){		
		this.todos = this.todosService.getTodos();
		this.todosDates = this.todosService.getUniqDates();
		this.checkPagination();		
	}

	goPagination(index: number){
	  	if(index === 1){
	  		this.pagFirst = 0;
	  		this.pagLast = 6;
	  	}else{
	  		this.pagLast = this.pagStep * index;
	  		this.pagFirst = this.pagLast - this.pagStep;  		
	  	}
	}

	checkPagination(){
		this.pagCount = new Array(Math.ceil(this.todos.length / this.pagStep));
	}

	activeClass(index: number){
		this.activeIndex = index;
	}	

	addTodo(todo: {name: string, text: string}){		
	  	this.todosService.addTodo(todo);		
		this.todos = this.todosService.getTodos();

		if(this.filterNameValue !== ''){
			this.filterTodo(false, this.activeIndex, this.pagCount.length);
			return;
		}

		this.checkPagination();		
	}	

	addFilterName(filterName){
		this.filterNameValue = filterName.name;		
		this.filterTodo();
	}

	addFilterDate(filterDate){
		this.filterDateValue = filterDate.date;	
		this.filterTodo();
	}

	filterTodo( flag = true, activePag = 1, countPag = 0){

		let searchName = this.filterNameValue;
		let searchDate = this.filterDateValue;	

		this.todos = this.todosService.getTodos();
		
		/* when search - go to first page */
		if(flag){			
			this.goPagination(activePag);
			this.activeClass(countPag);
		}
		
		if(this.todos.length === 0 || searchName === '' && searchDate === ''){			
			this.todos = this.todosService.getTodos();
			this.checkPagination();		
		}else{

			/* filtered todos */

			if(searchName !== '' && searchDate === ''){
				this.todos = this.todosService.filterName(searchName);
			}			

			if(searchName === '' && searchDate !== ''){				
				this.todos = this.todosService.filterDate(searchDate);				
			}	


			if(searchName !== '' && searchDate !== ''){
				this.todos = this.todosService.filterName(searchName);
				this.todos = this.todosService.filterDate(searchDate, this.todos);
			}

			this.checkPagination();						
		}	

		/* when delete filtered todos, pagination actions*/		
		if(this.pagCount.length === 1 && !flag){			
			this.goPagination(this.pagCount.length);
		}else{
			if((this.activeIndex + 1) > this.pagCount.length){
				this.goPagination(this.pagCount.length);
				this.activeClass(this.pagCount.length - 1);
			}			
		}		
	}

}