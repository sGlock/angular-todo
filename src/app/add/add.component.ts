import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
 
  constructor() { }

  ngOnInit() {
  }

  @Output() onAddTodo = new EventEmitter<{name: string, text: string}>();

  todo = {
  	todoName: '',
  	todoText: ''
  }  

  addTodo(){  	

    if(this.todo.todoName === '') this.todo.todoName = "Temp" + Date.now();
    if(this.todo.todoText === '') this.todo.todoText = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, facere.";

  	this.onAddTodo.emit({
  		name: this.todo.todoName,
  		text: this.todo.todoText
  	});

    this.todo.todoName = '';
    this.todo.todoText = '';
  }

}
