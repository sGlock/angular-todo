import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {

  @Input() todos: {name: string, text: string, date: string}
  @Input() pagStep: number
  @Input() pagCount: number
  @Input() activeIndex: number
  
  @Output() onGoPagination = new EventEmitter<{index: number}>();
  @Output() onActiveClass = new EventEmitter<{index: number}>();

  constructor() { }

  ngOnInit() {
  }

  activeClass(index){  	
  	this.onActiveClass.emit(index);
  }

  goPagination(index){  	
  	this.onGoPagination.emit(index);
  }

}
